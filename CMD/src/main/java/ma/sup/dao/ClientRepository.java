package ma.sup.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.sup.entities.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

}
