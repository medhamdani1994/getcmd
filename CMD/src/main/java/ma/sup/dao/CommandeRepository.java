package ma.sup.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.sup.entities.Commande;

public interface CommandeRepository extends JpaRepository<Commande, Long> {

}
