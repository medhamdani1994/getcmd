package ma.sup.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.sup.entities.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long> {

}
