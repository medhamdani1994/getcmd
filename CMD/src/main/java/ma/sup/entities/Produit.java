package ma.sup.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Produit implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	 private Long IdProduit;
	 private String description;
	 private String nomProduit;
	 private float prix;
	 
	 public Produit() {
	
	}
	 
	 
	public Produit(String description, String nomProduit, float prix) {
		
		this.description = description;
		this.nomProduit = nomProduit;
		this.prix = prix;
	}
	
	


	public Produit(String description, String nomProduit, float prix, List<Commande> commandes) {
	
		this.description = description;
		this.nomProduit = nomProduit;
		this.prix = prix;
		this.commandes = commandes;
	}




	@ManyToMany(fetch = FetchType.LAZY,
	            cascade = {
	                CascadeType.PERSIST,
	                CascadeType.MERGE
	            },
	            mappedBy = "produits")
	 private List<Commande> commandes;
	
	@JsonIgnore
	public List<Commande> getCommandes() {
		return commandes;
	}
	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}
	public Long getIdProduit() {
		return IdProduit;
	}
	public void setIdProduit(Long idProduit) {
		IdProduit = idProduit;
	}
	public String getNomProduit() {
		return nomProduit;
	}
	public void setNomProduit(String nomProduit) {
		this.nomProduit = nomProduit;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	} 

}
