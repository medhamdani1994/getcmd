package ma.sup.metier;

import java.util.List;
import java.util.Optional;

import ma.sup.entities.Client;

public interface ClientMetier {
	public Client saveClient(Client client);
	public List<Client> listClient();
	public Optional<Client> afficherClient(Long client);
	public void supprimerClient (Long client);
	public Client modifierClient(Client client);
	
}
