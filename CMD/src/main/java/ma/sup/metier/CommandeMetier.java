package ma.sup.metier;

import java.util.List;
import java.util.Optional;
import ma.sup.entities.Commande;

public interface CommandeMetier {
	public Commande saveCommande(Commande commande);
	public List<Commande> listCommande();
	public Optional<Commande> afficherCommande(Long commande);
	public void supprimerCommande (Long commande);
	public Commande modifierCommande(Commande commande);
}
