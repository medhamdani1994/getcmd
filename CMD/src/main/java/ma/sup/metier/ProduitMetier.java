package ma.sup.metier;

import java.util.List;
import java.util.Optional;

import ma.sup.entities.Produit;

public interface ProduitMetier {

	public Produit saveProduit(Produit produit);
	public List<Produit> listeProduits();
	public Optional<Produit> afficherProduit(Long produit);
	public void supprimerProduit (Long produit);
	public Produit modifierProduit(Produit produit);
	
	
	
}
