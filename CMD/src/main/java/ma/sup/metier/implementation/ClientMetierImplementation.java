package ma.sup.metier.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.sup.dao.ClientRepository;
import ma.sup.entities.Client;
import ma.sup.metier.ClientMetier;
@Service
public class ClientMetierImplementation implements ClientMetier {
	
	@Autowired
	private ClientRepository clientRepository;
	@Override
	public Client saveClient(Client client) {
		return clientRepository.saveAndFlush(client);
	}

	@Override
	public List<Client> listClient() {
		return clientRepository.findAll();
	}

	@Override
	public Optional<Client> afficherClient(Long client) {
		
		return clientRepository.findById(client);
	}

	@Override
	public void supprimerClient(Long client) {
		clientRepository.deleteById(client);
		
	}

	@Override
	public Client modifierClient(Client client) {
		return clientRepository.saveAndFlush(client);
	}

}
