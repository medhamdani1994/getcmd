package ma.sup.metier.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.sup.dao.CommandeRepository;
import ma.sup.entities.Commande;
import ma.sup.metier.CommandeMetier;

@Service
public class CommandeMetierImplementation implements CommandeMetier {
	
	@Autowired
	private CommandeRepository commandeRepository;
	
	@Override
	public Commande saveCommande(Commande commande) {
		
		return commandeRepository.saveAndFlush(commande);
	}

	@Override
	public List<Commande> listCommande() {
		
		return commandeRepository.findAll();
	}

	@Override
	public Optional<Commande> afficherCommande(Long commande) {
		
		return commandeRepository.findById(commande);
	}

	@Override
	public void supprimerCommande(Long commande) {
		commandeRepository.deleteById(commande);

	}

	@Override
	public Commande modifierCommande(Commande commande) {
		return commandeRepository.saveAndFlush(commande);
	}

}
