package ma.sup.metier.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.sup.dao.ProduitRepository;
import ma.sup.entities.Produit;
import ma.sup.metier.ProduitMetier;

@Service
public class ProduitMetierImplementation implements ProduitMetier {
	
	@Autowired
	ProduitRepository produitRepository;
	@Override
	public Produit saveProduit(Produit produit) {
		
		return produitRepository.saveAndFlush(produit);
	}

	@Override
	public List<Produit> listeProduits() {
		
		return produitRepository.findAll();
	}

	@Override
	public void supprimerProduit(Long produit) {
		produitRepository.deleteById(produit);
		
	}

	@Override
	public Produit modifierProduit(Produit produit) {
		return produitRepository.saveAndFlush(produit);
		
	}

	@Override
	public Optional<Produit> afficherProduit(Long produit) {
		return produitRepository.findById(produit);
	}

}
