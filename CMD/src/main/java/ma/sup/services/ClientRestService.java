package ma.sup.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ma.sup.entities.Client;
import ma.sup.metier.ClientMetier;

@RestController
public class ClientRestService {
	@Autowired
	private ClientMetier clientMetier;
	
	@PostMapping("/client")
	public Client saveClient(Client client) {
		return clientMetier.saveClient(client);
	}
	
	@GetMapping("/client")
	public List<Client> listClient() {
		return clientMetier.listClient();
	}
	
	@GetMapping("/client/{id}")
	public Optional<Client> afficherClient(@PathVariable("id") Long id) {
		return clientMetier.afficherClient(id);
	}
	
	@DeleteMapping("/client/{id}")
	public void supprimerClient(@PathVariable("id") Long id) {
		clientMetier.supprimerClient(id);
	}
	
	@PutMapping("/client/{id}")
	public Client modifierClient(@RequestBody Client client , @PathVariable("id")Long id) {
		client.setIdClient(id);
		return clientMetier.modifierClient(client);
	}
	
	
}
