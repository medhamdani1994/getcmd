package ma.sup.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ma.sup.entities.Commande;
import ma.sup.metier.CommandeMetier;

@RestController
public class CommandeRestService {
	@Autowired
	private CommandeMetier commandeMetier;

	@PostMapping("/commande")
	public Commande saveCommande(Commande commande) {
		return commandeMetier.saveCommande(commande);
	}
	
	@GetMapping("/commande")
	public List<Commande> listCommande() {
		return commandeMetier.listCommande();
	}
	
	@GetMapping("/commande/{id}")
	public Optional<Commande> afficherCommande(@PathVariable("id")  Long id) {
		return commandeMetier.afficherCommande(id);
	}
	
	@DeleteMapping("/commande/{id}")
	public void supprimerCommande(@PathVariable("id")  Long id) {
		commandeMetier.supprimerCommande(id);
	}
	
	@PutMapping("/commande/{id}")
	public Commande modifierCommande(@RequestBody Commande commande  , @PathVariable("id")Long id) {
		commande.setIdCommande(id);
		return commandeMetier.modifierCommande(commande);
	}
	
	
	

}
