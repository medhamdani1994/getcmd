package ma.sup.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ma.sup.entities.Produit;
import ma.sup.metier.ProduitMetier;

@RestController
public class ProduitRestService {
	@Autowired
	private ProduitMetier produitMetier;
	
	@PostMapping("/produit")
	public Produit saveProduit(Produit produit) {
		return produitMetier.saveProduit(produit);
	}
	
	@GetMapping("/produit")
	public List<Produit> listeProduits(){
		return produitMetier.listeProduits();
	}
	
	@PutMapping("/produit/{id}")
	public Produit modifierProduit(@RequestBody Produit produit , @PathVariable("id")Long id) {
		produit.setIdProduit(id);
		return produitMetier.modifierProduit(produit);
	}
	
	@DeleteMapping("/produit/{id}")
	public void supprimerProduit(@PathVariable("id") Long id) {
		produitMetier.supprimerProduit(id);
	}
	
	@GetMapping("/produit/{id}")
	public Optional<Produit> afficherProduit(@PathVariable("id") Long id) {
		return produitMetier.afficherProduit(id);
	}
}
